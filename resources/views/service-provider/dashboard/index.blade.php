@extends('service-provider.main')
@section('title','Dashboard')
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 blue"
                           href="{{ route('staff.appointment.history') }}">
                            <div class="visual">
                                <i class="fa fa-comments"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup"
                                          data-value="{{ $total_orders }}">{{ $total_orders }}</span>
                                </div>
                                <div class="desc">All Appointments</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 green-haze"
                           href="{{ url('staff/appointment-history?status=pending') }}">
                            <div class="visual">
                                <i class="fa fa-first-order"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup"
                                          data-value="{{ $pending_orders }}">{{ $pending_orders }}</span>
                                </div>
                                <div class="desc"> New Appointments</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 yellow"
                           href="{{ url('staff/appointment-history?status=accepted') }}">
                            <div class="visual">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup"
                                          data-value="{{ $accepted_orders->count() }}">{{ $accepted_orders->count() }}</span>
                                </div>
                                <div class="desc"> Accepted Appointments</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 green"
                           href="{{ url('staff/appointment-history?status=completed') }}">
                            <div class="visual">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup"
                                          data-value="{{ $completed_orders->count() }}">{{ $completed_orders->count() }}</span>
                                </div>
                                <div class="desc"> Completed Appointments</div>
                            </div>
                        </a>
                    </div>


                </div>
                @if(!empty($completed_orders) && (count($completed_orders) > 0))
                    <div class="row">
                        <div class="col-lg-12 col-xs-12 col-sm-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class=" icon-social-twitter font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Completed Orders</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_actions_pending">
                                            <!-- BEGIN: Actions -->
                                            <div class="mt-actions">

                                                @foreach($completed_orders as $completed_order)
                                                    <div class="mt-action">
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class="icon-magnet"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span
                                                                            class="mt-action-author">{{ $completed_order->user->fullName() }}</span>
                                                                        <p class="mt-action-desc">{{ ucfirst($completed_order->address) }}
                                                                            ,{{$completed_order->area->name}}
                                                                            ,{{$completed_order->city->name}}</p>
                                                                        <p class="mt-action-desc">{{ $completed_order->phone_number ?? '-' }}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span
                                                                        class="mt-action-date">{{ $completed_order->created_at->format('d/m/y') }}</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <a href="{{route('staff.user.appointment.show',$completed_order->id)}}"
                                                                           class="btn btn-outline green btn-sm">View
                                                                            Order</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>
                                            <!-- END: Actions -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('js')
@endpush
