@extends('service-provider.main')
@section('title','User Dairy')
@push('css')
    <style>
        .serviceInner {
            height: 280px;
        }

        .bannerFields {
            margin-top: 15px;
            margin-bottom: 30px;
        }

        .orderReviewDetails p {
            margin: 0;
        }

        .selectedServices {
            border: #ff6c2b solid 2px;
            padding: 15px 15px;
        }

        .btn:not(.btn-sm):not(.btn-lg) {
            line-height: 1.44;
            background-color: #ff6c2b;
        }

        .serviceBoxHeader {
            color: #fff;
        }

        /* Rating Star Widgets Style */
        .rating-stars {
            width: 100%;
        }

        .rating-stars ul {
            list-style-type: none;
            padding: 0;
            margin-top: 30px;

            -moz-user-select: none;
            -webkit-user-select: none;
        }

        .rating-stars ul > li.star {
            display: inline-block;

        }

        /* Idle State of the stars */
        .rating-stars ul > li.star > i.fa {
            font-size: 18px; /* Change the size of the stars */
            color: #ccc; /* Color on idle state */
        }

        /* Hover state of the stars */
        .rating-stars ul > li.star.hover > i.fa {
            color: #FFCC36;
        }

        /* Selected state of the stars */
        .rating-stars ul > li.star.selected > i.fa {
            color: #FF912C;
        }

        .ratingArea {
            margin-top: 30px;
        }

        .ratingArea h4 {
            font-weight: 700;
            margin: 10px 0;
            font-size: 16px;
        }

        .ratingArea img {
            object-fit: cover;
            width: 15%;
        }

        .modal .modal-content .modal-header {
            background: #ff6c2b;
        }

        .modal .modal-content .modal-header .modal-title {
            color: #fff;
        }

        @media screen and (max-width: 450px) {
            .orderReviewDetails h4 {
                margin: 5px 0;
            }
        }

        .mr-10 {
            margin-right: 10px;
        }

        .mb-15 {
            margin-bottom: 15px;
        }


    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__body">
                <div class="container">
                    <div class="serviceInnerMain">

                        <div class="serviceBoxMain">
                            <div class="serverInnerDetails">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 bannerFields">
                                        <div class="row">
                                            <div
                                                class="col-md-10 col-sm-10 col-xs-12 orderReviewDetails">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                        <p>
                                                            <b>Title: </b> {{ $dairy->title }}<br>
                                                        </p>
                                                        <p>
                                                            <strong>Date: </strong> {{date('d-m-Y',strtotime($dairy->created_at))}}
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <p><b>Description</b></p>
                                                        {!! $dairy->description !!}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>


                <!--end::Form-->
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                <div class="m-form__actions m-form__actions">
                    <a href="{{route('user.dairy.index',$dairy->user_id)}}" class="btn btn-info">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
    <script>
        $('#name').focusout(function () {

            var name = $(this).val();
            name = name.replace(/\s+/g, '-').toLowerCase();

            $('#slug').val(name);
        })
    </script>
@endpush
