@extends('service-provider.main')
@section('title','User Dairy')
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            User Dairy
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('user.dairy.create',$id) }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add User Dairy</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th> Sr NO.</th>
                        <th> Title</th>
                        <th> Date</th>
                        <th> Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($dairies))
                        @foreach($dairies as $dairy)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$dairy->title}}</td>
                                <td>{{date('d-m-Y',strtotime($dairy->created_at))}}</td>
                                <td>
                                    <a href="{{ route('user.dairy.show',$dairy->id) }}">View Detail</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                {orderable: false, targets: [3]}
            ],
        });
    </script>
@endpush
