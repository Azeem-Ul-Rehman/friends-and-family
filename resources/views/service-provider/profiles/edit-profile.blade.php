@extends('service-provider.main')
@section('title','Profile')
@push('css')
    <style>
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #fcefe1;
            opacity: 1;
        }
    </style>
@endpush
@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="">
                <div class="">
                    <div class="m-portlet">
                        <div class="container">
                            <div class="m-portlet__body">
                                <div class="serviceBoxMain">
                                    <div class="serverInnerDetails">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12 bannerFields">
                                                <h4>Edit Profile</h4>
                                                <form action="{{ route('staff.update.user.profile') }}"
                                                      method="post"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="first_name">First Name</label>
                                                                <input type="text" id="first_name" name="first_name"
                                                                       class="form-control"
                                                                       value="{{ $user->first_name }}">
                                                                @error('first_name')
                                                                <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="last_name">Last Name</label>
                                                                <input type="text" class="form-control"
                                                                       name="last_name" id="last_name"
                                                                       value="{{ $user->last_name }}">
                                                                @error('last_name')
                                                                <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="email">Email</label>
                                                                <input type="email" class="form-control" id="email"
                                                                       name="email" value="{{ $user->email }}">
                                                                @error('email')
                                                                <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Mobile No.</label>
                                                                <input type="text" name="phone_number"
                                                                       class="form-control disabledDiv"
                                                                       value="{{ $user->phone_number }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="cnic">CNIC </label>
                                                                <input type="text" class="form-control disabledDiv"
                                                                       name="cnic"
                                                                       id="cnic" value="{{ $user->cnic }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="">Gender </label>
                                                                <select class="form-control" name="gender">
                                                                    <option
                                                                        value="Male" {{ ($user->gender == 'Male') ? 'selected' : '' }}>
                                                                        Male
                                                                    </option>
                                                                    <option
                                                                        value="Female" {{ ($user->gender == 'Female') ? 'selected' : '' }}>
                                                                        Female
                                                                    </option>
                                                                </select>
                                                                @error('gender')
                                                                <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="city_id">City </label>
                                                                <select class="form-control cities"
                                                                        name="city_id"
                                                                        id="city_id">
                                                                    @foreach($cities as $city)
                                                                        <option
                                                                            value="{{ $city->id }}" {{ ($city->id == $user->city_id) ? 'selected' : ''}}>{{ ucfirst($city->name) }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="area_id">Area </label>
                                                                <select class="form-control areas"
                                                                        id="area_id"
                                                                        name="area_id">
                                                                    @foreach($areas as $area)
                                                                        <option
                                                                            value="{{ $area->id }}" {{ ($area->id == $user->area_id) ? 'selected' : ''}}>{{ ucfirst($area->name) }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="address">Address</label>
                                                                <input type="text" class="form-control"
                                                                       name="address" id="address"
                                                                       value="{{ $user->address }}">
                                                                @error('address')
                                                                <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12 text-right moreBtns">
                                                            <a href="{{ route('staff.profile.index') }}"
                                                               class="btn buttonMain hvr-bounce-to-right">
                                                                Cancel
                                                            </a>
                                                            <button type="submit"
                                                                    class="btn buttonMain hvr-bounce-to-right">Save
                                                                Changes
                                                            </button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')

    <script>

        $('.cities').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.areas';
            var city_id = $(this).val();
            var request = "city_id=" + city_id;

            if (city_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.cityAreas') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.area, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select Area</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select Area</option>");
            }
        });
    </script>
@endpush
