@extends('layouts.master')
@section('title','Orders')
@push('css')
    <style rel="stylesheet">
        .menuItemsCssClass input {
            width: 7%;
            float: left;
            margin-bottom: 15px;
        }

        .menuItemsCssClass span {
            padding-left: 20px;
            /*padding-top: 10px;*/
            display: block;
            margin-bottom: 15px;
        }

        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }

        .card-header a {
            text-decoration: none;
        }

        .accordion .card:first-of-type {
            border: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-header {
            background: #ff6c2b !important;
        }

        .mb-0 {
            color: #fff !important;
        }

        .orderModal .modal-header {
            background: #ff6c2b !important;
        }

        .orderModal .modal-title {
            color: #fff !important;
        }

        .removeBtn {
            float: right;
        }
    </style>
@endpush
@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">{{ __('Orders') }}</h3>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('Order') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.order.store') }}" id="add-orders"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <p><strong>Order Information</strong></p>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" name="grand_total" id="grand_total_price">
                                    <input type="hidden" value="0" name="delivery_charges" class="delivery_charges">
                                    <input type="hidden" value="0" name="time_zone" id="time_zone">

                                    <div id="priceofAllMenuItems">

                                    </div>
                                    <div id="typeOfAllMenuItems">

                                    </div>

                                    <div id="finalService">

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="customer_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Client') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="customer_id"
                                                    class="form-control customers @error('customer_id') is-invalid @enderror"
                                                    name="customer_id" autocomplete="customer_id">
                                                <option value="">Select an Client</option>
                                                @if(!empty($employees))
                                                    @foreach($employees as $employee)
                                                        <option
                                                            value="{{$employee->id}}"
                                                            data-delivery_charges="{{$employee->area['price']}}">{{ ucfirst($employee->fullName())}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('customer_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Categories') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="category_id"
                                                    class="form-control categories @error('category_id') is-invalid @enderror"
                                                    name="category_id[]" autocomplete="category_id">
                                                <option value="">Select a Category</option>
                                                @if(!empty($categories))
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}"
                                                                id="selectedCategory{{$category->id}}">{{ucfirst($category->name)}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            @error('category_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="service_category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Services') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="service_category_id"
                                                    class="form-control service_category @error('service_category_id') is-invalid @enderror js-example-basic-multiple"
                                                    name="service_category_id" autocomplete="service_category_id">

                                            </select>

                                            @error('service_category_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="menu_items_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Menu Items') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="menu_items_id"
                                                    class="form-control menu-items @error('menu_items_id') is-invalid @enderror"
                                                    name="menu_items_id" autocomplete="menu_items_id">

                                            </select>

                                            @error('menu_items_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4 disabledDiv">
                                            <label for="net_price"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Net Price') }}
                                                <span id="type-of-service"></span>
                                                <span class="mandatorySign">*</span></label>
                                            <input id="net_price" type="number"
                                                   class="form-control @error('net_price') is-invalid @enderror"
                                                   name="net_price" value="0"
                                                   autocomplete="net_price" autofocus>

                                            @error('net_price')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="datetimepicker1" class="col-md-4 col-form-label text-md-left">Date
                                                Time <span class="mandatorySign">*</span></label>

                                            <input type="text" name="datetimepicker1" id="datetimepicker1"
                                                   class="form-control @error('datetimepicker1') is-invalid @enderror"
                                                   required autocomplete="datetimepicker1">
                                            @error('datetimepicker1')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                        <div class="col-md-4">
                                            <label for="time" class="col-md-4 col-form-label text-md-left">Time <span
                                                    class="mandatorySign">*</span></label>

                                            <input type="text" name="time" id="time"
                                                   class="form-control @error('time') is-invalid @enderror"
                                                   required autocomplete="time">


                                            <span class="invalid-feedback" role="alert" style="display: none">
                                                    <strong>Please add time with gap of 2 hrs from now</strong>
                                                </span>
                                            @error('time')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6" style="display: none"  id="hidePaymentMode">
                                            <label for="time" class="col-md-4 col-form-label text-md-left">Payment Mode
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input type="radio" name="payment_mode" style="display: none"
                                                   class="form-control @error('payment_mode') is-invalid @enderror"
                                                   id="cash" value="Cash" > <span  id="cashText" style="display: none">Cash</span>
                                            <input type="radio" name="payment_mode" style="display: none"
                                                   class="form-control @error('payment_mode') is-invalid @enderror"
                                                   id="EasyPaisa" value="EasyPaisa"> <span id="EasyPaisaText" style="display: none">EasyPaisa</span>
                                            <input type="radio" name="payment_mode" style="display: none"
                                                   class="form-control @error('payment_mode') is-invalid @enderror"
                                                   id="Bank" value="Bank"> <span id="BankText" style="display: none">Bank</span>

                                            @error('payment_mode')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6" id="hideduration" style="display: none">
                                            <label for="duration"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Duration') }}
                                                <span class="mandatorySign">*</span></label>
                                            <select id="duration"
                                                    class="form-control duration @error('duration') is-invalid @enderror"
                                                    name="duration" autocomplete="duration">

                                                <option value="1">1 h</option>
                                                <option value="2">2 h</option>
                                                <option value="3">3 h</option>
                                                <option value="4">4 h</option>
                                                <option value="5">5 h</option>
                                                <option value="6">6 h</option>
                                                <option value="7">7 h</option>
                                                <option value="8">8 h</option>
                                                <option value="9">9 h</option>
                                                <option value="10">10 h</option>


                                            </select>

                                            @error('duration')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">

                                            <label for="alternate_address" class="col-md-4 col-form-label text-md-left">Alternate
                                                Address for This Service</label>
                                            <textarea class="form-control" id="alternate_address"
                                                      name="alternate_address"
                                                      rows="3"></textarea>

                                        </div>
                                    </div>

                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <p><strong>Customer Information</strong></p>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-4 disabledDiv">
                                            <label for="mobile_number"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Mobile Number') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="mobile_number" type="tel"
                                                   class="form-control @error('mobile_number') is-invalid @enderror"
                                                   name="mobile_number" value=""
                                                   autocomplete="mobile_number" autofocus>

                                            @error('mobile_number')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4 disabledDiv">
                                            <label for="city_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('City') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="city_name" type="text"
                                                   class="form-control @error('city_id') is-invalid @enderror"
                                                   name="city_name" value=""
                                                   autocomplete="city_name" autofocus>
                                            <input id="city_id" type="text" hidden
                                                   class="form-control @error('city_id') is-invalid @enderror"
                                                   name="city_id" value="0"
                                                   autocomplete="city_id" autofocus>

                                            @error('city_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4 disabledDiv">
                                            <label for="area_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Area') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="area_name" type="text"
                                                   class="form-control @error('area_name') is-invalid @enderror"
                                                   name="area_name" value=""
                                                   autocomplete="area_name" autofocus>
                                            <input id="area_id" type="text" hidden
                                                   class="form-control @error('area_id') is-invalid @enderror"
                                                   name="area_id" value="0"
                                                   autocomplete="area_id" autofocus>

                                            @error('area_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">

                                        <div class="col-md-12 disabledDiv">
                                            <label for="address"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Address') }}
                                                <span class="mandatorySign">*</span></label>
                                            <textarea id="address" type="text"
                                                      class="form-control @error('address') is-invalid @enderror"
                                                      name="address"
                                                      autocomplete="address" autofocus></textarea>

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-group row">


                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.get.order.history') }}" class="btn btn-info">Back</a>
                                    <input type="button" id="get-orders" class="btn btn-primary" disabled
                                           onclick="getOrder()" value="{{ __('SAVE') }}">

                                    </input>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('models')
    <!-- Modal -->
    <div class="modal fade orderModal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to confirm this order?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row subtotal_section">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                            <h5><strong>Sub Total </strong></h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                            <h5><strong>Rs. <span
                                        id="subtotal_price">0</span></strong>
                            </h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    {{--                    <div class="row referral_discount_section" style="display: none">--}}
                    {{--                        <div class="col-md-6 col-sm-6 col-xs-6 text-left">--}}
                    {{--                            <h5><strong>Referral Discount: </strong>(<span id="referral_discount_type"></span>)</h5>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">--}}
                    {{--                            <h5><strong>Rs. <span--}}
                    {{--                                        id="referral_discount">0</span></strong>--}}
                    {{--                            </h5>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="clearfix"></div>--}}
                    {{--                    </div>--}}

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                            <h5><strong>Delivery Charges: </strong>(<span>Fixed</span>)</h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                            <h5><strong>Rs. <span id="delivery_charges">0</span></strong>
                            </h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="row grand_total">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                            <h5><strong>Grand Total: </strong></h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                            <h5><strong>Rs. <span
                                        id="grand_total">0</span></strong>
                            </h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn buttonMain hvr-bounce-to-right" data-dismiss="modal">Close</button>
                    <a href="#" class="btn buttonMain hvr-bounce-to-right" id="order-submit">Yes</a>
                </div>
            </div>
        </div>
    </div>
@endpush

@push('js')
    <script>

        $(document).ready(function () {
            $('#time_zone').val(Intl.DateTimeFormat().resolvedOptions().timeZone);
            function dateTimePicker(dateID, timeID) {
                var dateToday = new Date();
                $(dateID).datetimepicker({
                    format: "MM-DD-YYYY",
                    minDate: moment().add(0, 'hours'),
                    icons: {
                        time: 'fa fa-clock',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-check',
                        clear: 'fa fa-trash',
                        close: 'fa fa-times'
                    }
                });
                var startTime = '09:15am';
                var endTime = '08:15pm';
                var startNewTime = moment(startTime, "HH:mm a");
                var endNewTime = moment(endTime, "HH:mm a");
                $(timeID).datetimepicker({
                    format: "hh:mm A",
                    stepping: 15, //will change increments to 15m, default is 1m
                    icons: {
                        time: 'fa fa-clock',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-check',
                        clear: 'fa fa-trash',
                        close: 'fa fa-times'
                    }
                }).on('dp.change', function (e) {
                    var date = e.date;//e.date is a moment object
                    var target = $(e.target).attr('name');
                    if (moment().format('MM-DD-YYYY') == $(dateID).val()) {
                        $('#time').val(date.add(0, 'hours').format('hh:mm A'));
                    } else {
                        $('#time').val(date.format('hh:mm A'));
                    }
                });
            }
            dateTimePicker('#datetimepicker1', '#time');
        });
        var delivery_charges = 0;
        $('.customers').select2({});
        $('.categories').select2();
        $('.service_category').select2();
    </script>
    <script>
        $('.customers').change(function () {
            form = $(this).closest('form');
            node = $(this);

            var customer_id = $(this).val();
            var request = "customer_id=" + customer_id;
            $('#net_price').val(0);
            $('#grand_total').val(0);

            if (customer_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.customerServiceCategory') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $('#mobile_number').val(response.data.user.phone_number);
                            $('#address').val(response.data.user.address);
                            $('#city_id').val(response.data.user.city_id);
                            $('#area_id').val(response.data.user.area_id);
                            $('#city_name').val(response.data.city_name);
                            $('#area_name').val(response.data.area_name);
                            $('#alternate_address').val(response.data.user.address);
                            $('.delivery_charges').val(response.data.area_price);

                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please select Client");
            }
        });
        $('.categories').change(function () {



            $('#finalService').empty();
            $('.service_category').empty();
            $('.menu-items').empty();
            $("#priceofAllMenuItems").empty();
            $("#typeOfAllMenuItems").empty();
            $('#net_price').val(0);
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.service_category';
            var category_id = $(this).val();
            if(category_id == '1'){
                $('#hidePaymentMode').show();
                $('#cash').show();
                $('#cashText').show();
                $('#EasyPaisa').hide();
                $('#EasyPaisaText').hide();
                $('#Bank').hide();
                $('#BankText').hide();
                $('#cash').attr('checked',true).trigger('change');
                $('#EasyPaisa').attr('checked',false).trigger('change');
                $('#Bank').attr('checked',false).trigger('change');
            }else if(category_id == '2'){
                $('#cash').hide();
                $('#cashText').hide();
                $('#hidePaymentMode').show();
                $('#EasyPaisa').show();
                $('#EasyPaisaText').show();
                $('#Bank').show();
                $('#BankText').show();
                $('#cash').attr('checked',false).trigger('change');
                $('#EasyPaisa').attr('checked',false).trigger('change');
                $('#Bank').attr('checked',true).trigger('change');
            }else{
                $('#cash').hide();
                $('#cashText').hide();
                $('#EasyPaisa').hide();
                $('#EasyPaisaText').hide();
                $('#Bank').hide();
                $('#BankText').hide();
                $('#cash').attr('checked',false).trigger('change');
                $('#EasyPaisa').attr('checked',false).trigger('change');
                $('#Bank').attr('checked',false).trigger('change');
            }
            var request = "category_id=" + category_id;
            if (category_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.serviceCategory') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";

                            $.each(response.data.service_category, function (i, obj) {
                                html += '<option value="' + obj.id + '" id="selectedServices' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select Services</option>");
                            $('.js-example-basic-multiple').select2();
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value=''>Select Services</option>");
            }
        });
        $('.service_category').change(function () {
            $('#finalService').empty();
            $('.menu-items').empty();
            $('#net_price').val(0);
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.menu-items';
            var service_id = $(this).val();
            var request = "service_id=" + service_id;
            $('#net_price').val(0);
            $('#grand_total').val(0);

            if (service_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.serviceSubCategory') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            var priceInputHtml = "";
                            var typeInputHtml = "";

                            html += '<option value="" selected>Select Menu Items</option>';
                            $.each(response.data.sub_category, function (i, obj) {
                                html += '<option value="' + obj.id + '" id="selectedMenuItem' + obj.id + '">' + obj.name + '</option>';
                                priceInputHtml += '<input type="hidden" value="' + obj.price + '" id="selectedMenuItemPrice' + obj.id + '"/>'
                                typeInputHtml += '<input type="hidden" value="' + obj.service_type + '" id="selectedMenuItemType' + obj.id + '"/>'
                            });
                            $(node_to_modify).html(html);
                            $("#priceofAllMenuItems").html(priceInputHtml);
                            $("#typeOfAllMenuItems").html(typeInputHtml);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value=''disabled>Select Menu Items</option>");
            }
        });
        $('.menu-items').change(function () {

            var category_id = $('#category_id').val();
            var category_text = $('#category_id option:selected').text();

            var service_id = $('#service_category_id').val();
            var service_text = $('#service_category_id option:selected').text();
            var menu_item_id = $('#menu_items_id').val();
            var menu_item_text = $('#selectedMenuItem' + menu_item_id + '').text();
            var menu_item_price = $('#selectedMenuItemPrice' + menu_item_id + '').val();
            var menu_item_type = $('#selectedMenuItemType' + menu_item_id + '').val();


            if (menu_item_id !== '') {
                $("#get-orders").removeAttr("disabled");
                var net_price = $('#net_price').val();
                $('#net_price').val(parseInt(net_price) + parseInt(menu_item_price));
                // $('#total_packages_price').text($('#net_price').val());
                dynamic_field(category_id, category_text, service_id, service_text, menu_item_id, menu_item_text, menu_item_price, menu_item_type);
            } else {
                $("#get-orders").attr("disabled", true);
                var net_price = $('#net_price').val();
                $('#net_price').val(0);
                // $('#total_packages_price').text($('#net_price').val());
                // $('#boxwithCategoryService' + category_id + '' + service_id + '' + oldUnselectValue + '').remove();
            }
        });
        $('.duration').change(function () {
            var menu_item_id = $('#menu_items_id').val();
            var menu_item_price = $('#selectedMenuItemPrice' + menu_item_id + '').val();
            var duration = $(this).val();
            $('#net_price').val(parseInt(duration) * parseInt(menu_item_price));
        });
        function getOrder() {
            var netPrice = parseInt($('#net_price').val());
            var category_text = $('#category_id option:selected').text();

            if (category_text == 'Online') {
                var delivery_charges = 0;
            } else {
                var delivery_charges = parseInt($('.delivery_charges').val());
            }

            $('#delivery_charges').text(delivery_charges);
            $('.delivery_charges').val(delivery_charges);
            $('#subtotal_price').text(netPrice);
            netPrice = netPrice + delivery_charges;
            $('#grand_total').text(netPrice);
            $('#grand_total_price').val(netPrice);
            $('#exampleModal').modal('show');
        }
        $('#order-submit').click(function (e) {


            e.preventDefault();
            let form = $('#add-orders');
            var request = $(form).serialize();

            $.ajax({
                type: "POST",
                url: "{{ route('admin.order.store') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    toastr[response.data.flash_status](response.data.flash_message);

                    if (response.data.flash_status == "success") {
                        window.location = '{{route('admin.get.order.history')}}';
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        });
    </script>
    <script>
        var count = 1;
        function f(category_id, category_text, service_id, service_text, menu_item_id, menu_item_text, menu_item_price, menu_item_type) {
            // html = '<tr id="boxwithCategoryService' + category_id + '' + service_id + '">';
            // html += '<td style="display: none"><input type="hidden" name="selected_category_id[]" class="form-control" value="' + category_id + '" /></td>';
            // html += '<td style="display: none"><input type="hidden" name="selected_service_id[' + category_id + '][]" class="form-control" value="' + service_id + '" /></td>';
            // html += '<td style="display: none"><input type="hidden" name="selected_service_price[' + category_id + '][' + service_id + ']" class="form-control" value="' + service_price + '" /></td>';
            // html += '<td><input type="text"   class="form-control"   value="' + category_text + '" readonly/></td>';
            // html += '<td><input type="text"   class="form-control"   value="' + service_text + '" readonly/></td>';
            // html += '<td><input type="text"   class="form-control"   value="' + service_price + '"readonly/></td>';
            // html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove" onclick="removeSelectedService(' + category_id + ',' + service_id + ',' + service_price + ')">Remove</button></td></tr>';
            // $('tbody').append(html);
        }
        function dynamic_field(category_id, category_text, service_id, service_text, menu_item_id, menu_item_text, menu_item_price, menu_item_type) {

            var type = ''
            if (menu_item_type == 'hourly') {

                type = 'hour';
                $('#hideduration').show();
            } else {
                type = 'fixed';
                $('#hideduration').hide();
            }
            if (category_text == 'Online') {
                category_text = 'online';
            } else {
                category_text = 'onsite';
            }

            $('#type-of-service').text('/' + type);
            var html = '';
            html += '<input type="hidden" name="selected_category_id[]" class="form-control" value="' + category_id + '"/>';
            html += '<input type="hidden" name="selected_category_name" class="form-control" value="' + category_text + '"/>';
            html += '<input type="hidden" name="selected_service_id[' + category_id + '][]" class="form-control" value="' + service_id + '"/>';
            html += '<input type="hidden" name="selected_menu_item_id[' + category_id + '][' + service_id + '][]" class="form-control" value="' + menu_item_id + '"/>';
            html += '<input type="hidden" name="selected_menu_item_price[' + category_id + '][' + service_id + '][' + menu_item_id + ']" class="form-control" value="' + menu_item_price + '"/>';
            html += '<input type="hidden" name="selected_menu_item_type[' + category_id + '][' + service_id + '][' + menu_item_id + ']" class="form-control" value="' + menu_item_type + '"/>';


            $('#finalService').html(html);


            // var current_address = $('#address').val();
            // var html = "";
            // html += ' <div class="check-duplicates" data-duplicate="' + category_id + '' + service_id + '' + menu_item_id + '" id="boxwithCategoryService' + category_id + '' + service_id + '' + menu_item_id + '">';
            // html += '<input type="hidden" name="selected_category_id[]" class="form-control" value="' + category_id + '"/>';
            // html += '<input type="hidden" name="selected_service_id[' + category_id + '][]" class="form-control" value="' + service_id + '"/>';
            // html += '<input type="hidden" name="selected_menu_item_id[' + category_id + '][' + service_id + '][]" class="form-control" value="' + menu_item_id + '"/>';
            // html += '<input type="hidden" name="selected_menu_item_price[' + category_id + '][' + service_id + '][' + menu_item_id + ']" class="form-control" value="' + menu_item_price + '"/>';
            // html += '<input type="hidden" name="selected_menu_item_type[' + category_id + '][' + service_id + '][' + menu_item_id + ']" class="form-control" value="' + menu_item_type + '"/>';
            //
            // html += '<div class="form-group row">';
            // html += '<div class="col-md-12">';
            // html += '<button type="button" name="remove"  id="" class="btn btn-danger remove removeBtn" onclick="removeSelectedService(' + category_id + ',' + service_id + ',' + menu_item_id + ',' + menu_item_price + ')">Remove </button>';
            // html += '</div>';
            // html += '</div>';
            //
            //
            // html += '<div class="form-group row">';
            //
            // html += '<div class="col-md-4 disabledDiv">';
            // html += '<label  class="col-md-4 col-form-label text-md-left">Category Name</label>';
            // html += '<input type="text" class="form-control" value="' + category_text + '">';
            // html += '</div>';
            //
            // html += '<div class="col-md-4 disabledDiv">';
            // html += '<label  class="col-md-4 col-form-label text-md-left">Service Name</label>';
            // html += '<input type="text" class="form-control" value="' + service_text + '" readonly/>';
            // html += '</div>';
            //
            // html += '<div class="col-md-4 disabledDiv">';
            // html += '<label  class="col-md-6 col-form-label text-md-left">Menu Item Name</label>';
            // html += '<input type="text" class="form-control" value="' + menu_item_text + '" readonly/>';
            // html += '</div>';
            //
            //
            // html += '</div>';
            //
            // html += '<div class="form-group row">';
            //
            //
            // html += '<div class="col-md-4 disabledDiv">';
            // html += '<label  class="col-md-6 col-form-label text-md-left">Price / ' + type + '</label>';
            // html += '<input type="text" class="form-control" value="' + menu_item_price + '" readonly/>';
            // html += '</div>';
            //
            // html += '<div class="col-md-4">';
            // html += '<label for="datetimepicker1" class="col-md-4 col-form-label text-md-left">Date<span class="mandatorySign">*</span></label>';
            // html += '<input type="text" name="selected_service_datetimepicker1[' + category_id + '][' + service_id + '][' + menu_item_id + ']" id="selected_service_datetimepicker1' + category_id + '' + service_id + '' + menu_item_id + '" class="form-control " required autocomplete="datetimepicker1" value="">';
            // html += '</div>';
            //
            // html += '<div class="col-md-4">';
            // html += '<label for="time" class="col-md-4 col-form-label text-md-left">Time<span class="mandatorySign">*</span></label>';
            // html += '<input type="text" name="selected_service_time[' + category_id + '][' + service_id + '][' + menu_item_id + ']" id="selected_service_time' + category_id + '' + service_id + '' + menu_item_id + '" class="form-control " required autocomplete="time" value="">';
            // html += '</div>';
            //
            // html += '</div>';
            //
            //
            // html += '<div class="form-group row">';
            //
            // html += '<div class="col-md-12">';
            // html += '<label for="alternate_address" class="col-md-4 col-form-label text-md-left">Alternate Address </label>';
            // html += '<textarea class="form-control" id="alternate_address"  name="selected_service_alternate_address[' + category_id + '][' + service_id + '][' + menu_item_id + ']" rows="3">' + current_address + '</textarea>';
            // html += '</div>';
            //
            // html += '</div>';
            //
            // html += '<hr>';
            // html += '</div>'
            // $('.service-append').append(html);
            // dateTimePicker('#selected_service_datetimepicker1' + category_id + '' + service_id + '' + menu_item_id + '', '#selected_service_time' + category_id + '' + service_id + '' + menu_item_id + '');


        }
        function removeSelectedService(category_id, service_id, menu_item_id, menu_item_price) {
            $('#boxwithCategoryService' + category_id + '' + service_id + '' + menu_item_id + '').remove();
            $('#selectedMenuItem' + menu_item_id + '').prop("selected", false);
            $('#selectedMenuItem' + menu_item_id + '').trigger("change");
            var net_price = $('#net_price').val();
            $('#net_price').val(parseInt(net_price) - parseInt(menu_item_price));
            $('#total_packages_price').text($('#net_price').val());
        }
    </script>

@endpush
<style>
    .bootstrap-datetimepicker-widget {

        width: 266px !important;
    }

</style>


