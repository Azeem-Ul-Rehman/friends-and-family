@extends('frontend.layout.app')
@push('css')
@endpush
@section('banner')
    <div class="innerBannerContent">
        <div class="container">
            <h2>FAQ</h2>
            <p>Smart session anywhere, anytime.</p>
        </div>
    </div>

@endsection
@section('content')
    <div class="aboutInner">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h3 class="sectionHeading">Have Any Questions ?</h3>
                    <p>FF works to aid human beings in some capacity. For us your mental, emotional and physical health
                        is just as unique and important as you are. We believe in delivering safe, effective care to
                        each client that entrusts us with their health. Depending on the job a Social Worker might do
                        many different tasks but all of them will have the end goal of meeting the individual needs of
                        their client. FF services at its heart are about expressing an inner desire to help fellow human
                        beings in society. Maintaining privacy and confidentiality is our priority. And we believe
                        clients should be treated as autonomous agents able to exercise their autonomy to the fullest
                        extent possible, including the right to privacy and the right to have private information
                        remains confidential. Depending upon the selected service by the client the FF team will ensure
                        to help and assess the Bio-Psycho-social needs of client and connect them to the appropriate
                        Social Worker available for specific service. </p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:75px;">
                    <div class="panel-group" id="accordion">

                        @foreach($faqs as $key =>$faq)
                            <div class="panel panel-default">
                                <div class="panel-heading {{ ($loop->index == 0) ?'active' : '' }}">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse{{$key}}">
                                            <span>?</span> {{ $faq->title }}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$key}}"
                                     class="panel-collapse collapse {{ ($loop->index == 0) ?'in' : '' }}">
                                    <div class="panel-body">
                                        {{ $faq->description }}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')
@endpush
