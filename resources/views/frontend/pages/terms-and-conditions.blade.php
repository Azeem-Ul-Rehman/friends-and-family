@extends('frontend.layout.app')
@push('css')
@endpush
@section('banner')
    <div class="innerBannerContent">
        <div class="container">
            <h2>Terms & Conditions</h2>
        </div>
    </div>

@endsection
@section('content')

    <div class="aboutInner">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! ($terms) ? $terms->description : '' !!}
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')
@endpush

