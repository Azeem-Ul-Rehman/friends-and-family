@php
    $aboutus =\App\Models\Aboutus::first();

@endphp

<div class="about">
    <div class="container">
        <div class="text-center">
            <h3 class="sectionHeading">About Us</h3>
            <p>{!! ($aboutus) ? $aboutus->summary : ''!!}</p>
            <a href="{{ route('aboutus.detail') }}" class="btn btn-primary btnMain">View More</a>
        </div>
    </div>
</div>
