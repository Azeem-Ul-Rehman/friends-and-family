<ul class="nav navbar-nav navbar-right">
    <li class="{{ (request()->is('/')) ? 'activeNav' : '' }}"><a href="{{ route('index') }}">Home</a></li>
    <li class="{{ (request()->is('about-us')) ? 'activeNav' : '' }}"><a href="{{ route('aboutus.detail') }}">About Us</a></li>
    <li class="{{ (request()->is('services')) ? 'activeNav' : '' }}"><a href="{{ route('service') }}">Services</a></li>
    <li class="{{ (request()->is('faqs')) ? 'activeNav' : '' }}"><a  href="{{ route('faqs') }}">FAQ</a></li>
    <li class="{{ (request()->is('contacts')) ? 'activeNav' : '' }} hidden-xs"><a href="{{ route('contacts.create') }}">Contact Us</a></li>
</ul>
