@php
    $settings =\App\Models\Setting::all();
@endphp
<div class="navbar-fixed-top">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a class="navbar-brand" href="{{ route('index') }}"><img src="{{ asset('frontend/images/logo.png') }}"
                                                                         alt=""></a></div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="defaultNavbar1">
                <div class="topbar">
                    <ul class="list-unstyled list-inline">
                        @guest
                            <li>
                                <a href="{{ route('login') }}">
                                    <img src="{{ asset('frontend/images/ff.png') }}" alt="">
                                    Join as Family Friends
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('login') }}">
                                    <img src="{{ asset('frontend/images/acountIcon.png') }}"
                                         alt=""> My Account
                                </a>
                            </li>
                        @endguest
                        @auth
                            @if(auth()->user()->hasRole('admin'))
                                <li>
                                    <a href="{{ route('admin.dashboard.index') }}">
                                        <img src="{{ asset('frontend/images/acountIcon.png') }}"
                                             alt=""> My Account
                                    </a>
                                </li>
                            @elseif(auth()->user()->hasRole('staff'))
                                <li>
                                    <a href="{{ route('staff.dashboard.index') }}">
                                        <img src="{{ asset('frontend/images/acountIcon.png') }}"
                                             alt=""> My Account
                                    </a>
                                </li>
                            @elseif(auth()->user()->hasRole('customer'))
                                <li>
                                    <a href="{{ route('customer.dashboard.index') }}">
                                        <img src="{{ asset('frontend/images/acountIcon.png') }}"
                                             alt=""> My Account
                                    </a>
                                </li>
                            @endif

                            <li class="logoutLink"><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><img
                                        src="{{ asset('frontend/images/logout.png') }}"
                                        alt="">Logout</a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @endauth
                        <li><a href="{{$settings[10]->value}}"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="{{$settings[9]->value}}"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="{{$settings[8]->value}}"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                @include('frontend.include.navbar')
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</div>
