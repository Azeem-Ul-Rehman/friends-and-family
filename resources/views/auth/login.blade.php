@extends('frontend.layout.app')
@section('title','Login')

@section('banner')
    <div class="innerBannerContent fieldsBannerContent">
        <div class="container">
            <h2>Login</h2>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <form method="POST" action="{{ route('login') }}" autocomplete="off">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Enter your Phone No</label>
                                    <input type="text" class="form-control @error('phone_number') is-invalid @enderror"
                                           name="phone_number" id="phone_number" value="{{ old('phone_number') }}"
                                           maxlength="11" autocomplete="phone_number"
                                           placeholder="03001234567" pattern="[03]{2}[0-9]{9}"
                                           onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                                           title="Phone number with 03 and remaing 9 digit with 0-9"
                                           autofocus>
                                    @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Enter your Password</label>
                                    <input d="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           required autocomplete="current-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <input class="form-check-input" type="checkbox" name="remember"
                                           id="remember" {{ old('remember') ? 'checked' : '' }} value="1">
                                    <label class="form-check-label" for="remember">
                                          {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btnMain btnDetails">Sign in</button>
                                </div>
                            </div>
                            <div class="col-md-12 forgotPass">
                                @if (Route::has('password.request'))
                                    <p><a href="{{ route('password.request') }}">{{ __('Forgot Password?') }}</a></p>
                                @endif
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12" >
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <h4>Don't have an account? Sign Up</h4>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="{{ route('register') }}" class="btn btn-primary btnMain btnDetails" style="width: 100% !important;">As Customer</a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="{{ route('register.family.friend') }}" class="btn btn-primary btnMain btnDetails" style="width: 100% !important;">As Staff</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
{{--                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">--}}
{{--                                <a href="#"><img src="{{ asset('frontend/images/fb.png') }}" alt="facebook"></a>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">--}}
{{--                                <a href="#"><img src="{{ asset('frontend/images/gp.png') }}" alt="google"></a>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-12">--}}
{{--                                <p>Don't have an account? <a href="{{ route('register') }}">Sign Up</a></p>--}}
{{--                            </div>--}}

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
@endsection

@push('js')
    <script>

        $('#phone_number').focusout(function () {
            if (/^(03)\d{9}$/.test($(this).val())) {
                // value is ok, use it
            } else {

            }
        });
    </script>
@endpush
