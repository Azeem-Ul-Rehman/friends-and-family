<?php

namespace App\Http\Controllers;
use App\Models\Banner;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {

        $banners = Banner::all();
        return view('welcome',compact('banners'));
    }
}
