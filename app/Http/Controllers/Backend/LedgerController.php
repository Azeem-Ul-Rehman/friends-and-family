<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderReceivedAmount;
use Illuminate\Http\Request;


class LedgerController extends Controller
{

    public function index()
    {
        $orders =Order::with(['user','staff.comission'])->where('order_status','completed')->where('staff_status','completed')->get();
        return view('backend.ledger.index', compact('orders'));
    }
}
