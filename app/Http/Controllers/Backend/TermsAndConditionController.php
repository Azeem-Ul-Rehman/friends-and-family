<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\TermAndConditions;
use Illuminate\Http\Request;

class TermsAndConditionController extends Controller
{
    public function index()
    {
        $terms = TermAndConditions::first();
        return view('backend.terms-and-conditions.create', compact('terms'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
        ], [
            'description.required' => 'Description is required.'
        ]);
        $terms = TermAndConditions::first();
        if (!is_null($terms)) {
            $terms->update([
                'description' => $request->description
            ]);
        } else {
            $terms = new TermAndConditions();
            $terms->description = $request->description;
            $terms->save();
        }


        return back()->with([
            'flash_status' => 'success',
            'flash_message' => 'Description updated successfully.'
        ]);
    }
}
