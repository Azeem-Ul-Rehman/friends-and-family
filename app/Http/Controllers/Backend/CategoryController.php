<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $services = Category::orderBy('id', 'DESC')->get();

        return view('backend.category.index', compact('services'));
    }

    public function create()
    {
        return view('backend.category.create');

    }
    public function show($id){

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        Category::create($request->all());
        return redirect()->route('admin.category.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'ServiceCategory created successfully.'
            ]);

    }

    public function edit($id)
    {
        $service = Category::find($id);
        return view('backend.category.edit', compact('service'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $category = Category::find($id);

        $category->update([
            'name' => $request->name,
        ]);
        return redirect()->route('admin.category.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'ServiceCategory updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $service = Category::findOrFail($id);
        $service->delete();

        return redirect()->route('admin.category.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'ServiceCategory has been deleted'
            ]);
    }
}
