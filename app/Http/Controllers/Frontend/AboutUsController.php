<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Aboutus;
use App\Models\ManagingPartener;
use App\Models\PrivacyPolicy;
use App\Models\TermAndConditions;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index()
    {
        $aboutus = Aboutus::first();
        $teams = ManagingPartener::all();
        return view('frontend.pages.aboutus-detail', compact('aboutus', 'teams'));
    }

    public function privacyPolicy()
    {
        $privacyPolicy = PrivacyPolicy::first();
        return view('frontend.pages.privacy-policy', compact('privacyPolicy'));
    }

    public function terms()
    {
        $terms = TermAndConditions::first();
        return view('frontend.pages.terms-and-conditions', compact('terms'));
    }
}
