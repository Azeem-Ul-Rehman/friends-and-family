<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermAndConditions extends Model
{
    protected $table = "terms_and_conditions";
    protected $guarded = [];
}
